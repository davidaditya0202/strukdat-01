/**
*Author : David Aditya Susanto
*NPM : 140810190067
*Deskripsi : Membuat program menukar celsius ke reamur
*tanggal :  26 Februari 2020
*/
#include <iostream>
using namespace std;
 
float cel2re(int celsius)
{
	return (celsius*4)/5; 
}
 
int main()
{
    int celsius = 5;
    float reamur = cel2re(celsius);
    cout<<"Reamur : "<<reamur;
    return 0;
}
