/**
*Author : David Aditya Susanto
*NPM : 140810190067
*Deskripsi : Membuat program pengurutan angka
*tanggal :  26 Februari 2020
*/
#include <iostream>
using namespace std;
 
void sortArr(int arr[], int n)
{
  int i, j, tmp;
  for (i = 0; i < n; i++){
    for (j = 0; j < n - i - 1; j++){
      if (arr[j] > arr[j + 1]){
        tmp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = tmp;
      }
    }
  }
}
 
int main()
{
    int arr[] = {6, 9, 3, 1, 7, 5};
    int n = sizeof(arr)/sizeof(arr[0]);
    sortArr(arr, n);
    for (int i=0;i<n;i++){
        cout<<arr[i]<<" ";
    }
    return 0;
}
