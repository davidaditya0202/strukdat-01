/**
*Author : David Aditya Susanto
*NPM : 140810190067
*Deskripsi : Membuat program pembalik kata
*tanggal :  26 Februari 2020
*/

#include<iostream>
using namespace std;

void reverse(string& kata, int panjang)
{
	panjang = kata.length();
  	int n = panjang - 1;
  	for(int i=0;i<(panjang/2);i++){
    char temp = kata[i];
    kata[i] = kata[n];
    kata[n] = temp;
    n = n-1;
  }
}

int main()
{
    string kata = "howdy";
    reverse(kata, kata.length());
    cout<<kata;
    return 0;
}
