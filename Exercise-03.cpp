/**
*Author : David Aditya Susanto
*NPM : 140810190067
*Deskripsi : Membuat program penukar posisi angka
*tanggal :  26 Februari 2020
*/
#include <iostream>
using namespace std;
 
void swap(int& x, int& y) {
    int temp = x; 
    x = y; 
    y = temp; 
}
 
int main()
{
    int x = 7;
    int y = 12;
    cout << "x is " << x << endl;
    cout << "y is " << y << endl;
    swap(x, y);
    cout << "After swap" << endl;
    cout << "x is " << x << endl;
    cout << "y is " << y << endl;
    return 0;
}
